package com.unox.imagedownloaderexample.models

import android.graphics.Bitmap

class RemoteImage(val url: String) {
    var bitmapRes: Bitmap? = null
    var loadingError: Boolean = false
}