package com.unox.imagedownloaderexample

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.Image
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.unox.imagedownloaderexample.models.RemoteImage
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.image_container.view.*
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.Executors
import kotlin.random.Random

val imagesUrls = listOf(
    "https://banner2.kisspng.com/20171202/e3e/fruit-free-png-image-5a22fd776d6943.7240975215122425514482.jpg",
    "http://www.pngmart.com/files/5/Red-Apple-PNG-Photos.png",
    "https://www.clipartmax.com/png/middle/66-661522_rip-banana-fruit-transparent-background-png-image-banana-png.png",
    "https://banner2.kisspng.com/20171127/d9f/fruit-basket-png-vector-clipart-image-5a1c01ee7dd394.8919843915117849425154.jpg"
)

var imagesList = MutableList(imagesUrls.size) {
    RemoteImage(imagesUrls[it])
}

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = MyAdapter(imagesList)
        val layoutManager = GridLayoutManager(this, 3)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager


        // This callback will receive updates as images downloads finish
        ImageDownloadManager.setDownloadListener(object: ImageDownloadManager.ImageDownloadListener {
            override fun onDownloadComplete(requestCode: Int, image: Bitmap) {
                imagesList[requestCode].apply {
                    bitmapRes = image
                    loadingError = false
                }
                adapter.notifyItemChanged(requestCode)
            }

            override fun onDownloadFailed(requestCode: Int) {
                imagesList[requestCode].apply {
                    bitmapRes = null
                    loadingError = true
                }

                adapter.notifyItemChanged(requestCode)

            }
        })


        button_singletask_load.setOnClickListener {
            // Reset all images in the list
            imagesList.forEach {
                it.bitmapRes = null
                it.loadingError = false

            }
            adapter.notifyDataSetChanged()

            // Start the download of each image
            imagesUrls.forEachIndexed { index, url -> ImageDownloadManager.downloadImage(index, url) }
        }

    }
}


/**
 * Adapter class that simply renders an ImageView and a circular ProgressBar while a download is running.
 * During the download or when a download failed, an image placeholder is used instead of the actual picture.
 */
class MyAdapter(private val dataset: List<RemoteImage>) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        val imageView = view.imageView
        val progress = view.progressBar
    }

    override fun getItemCount(): Int {
        return dataset.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (dataset[position].bitmapRes != null) {
            holder.imageView.setImageBitmap(dataset[position].bitmapRes)
            holder.progress.visibility = View.GONE
        } else {
            holder.progress.visibility = if (dataset[position].loadingError) View.GONE else View.VISIBLE
            holder.imageView.setImageResource(R.drawable.ic_image_black_24dp)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.image_container, parent, false)
        return ViewHolder(view)
    }
}




object ImageDownloadManager {
    private var listener: ImageDownloadListener? = null

    /**
     * Try to change the number of threads in the Thread pool and see the impact on the images loading process.
     * If set to 1, it will load 1 image at a time like AsyncTasks do by default
     */
    private val executor = Executors.newFixedThreadPool(4)

    /** Status of the download */
    private val DOWNLOAD_COMPLETED = 1
    private val DOWNLOAD_FAILED = 0


    /**
     * Creates a Handler bound to the UI thread. It will receive messages containing finished ImageDownloadTask objects.
     * Since it is bound to the UI thread, once the listener receives the onDownloadComplete callback it can safely
     * interact with UI objects
     */
    private val uiHandler = Handler(Looper.getMainLooper()) {
        val task = it.obj as ImageDownloadTask

        when (it.what) {
            DOWNLOAD_COMPLETED -> listener?.onDownloadComplete(task.requestCode, task.bitmap!!)
            DOWNLOAD_FAILED    -> listener?.onDownloadFailed(task.requestCode)
        }

        true
    }


    /**
     * Request the download of an image resource from a remote url
     * @param requestCode Code used to identified this particular request.
     * @param urlString String containing the URL of the remote image.
     */
    fun downloadImage(requestCode: Int, urlString: String) {
        executor.execute(ImageDownloadTask(URL(urlString), requestCode))
    }


    /**
     * Sets a listener for being notified when a download finished
     */
    fun setDownloadListener(listener: ImageDownloadListener) {
        this.listener = listener
    }


    /**
     * When an ImageDownloadTask finishes, it notifies the manager invoking this method
     * @param task The task that finished
     * @param state Either DOWNLOAD_COMPLETED or DOWNLOAD_FAILED
     */
    private fun onTaskFinish(task: ImageDownloadTask, state: Int) {
        // Pass the event to the Main Thread loop
        uiHandler.obtainMessage(state, task).sendToTarget()
    }


    /**
     * This interface is implemented by the caller to receive updates when a download finishes
     */
    interface ImageDownloadListener {
        /**
         * Invoked when a download task completes successfully.
         * @param requestCode Identifier of the download request
         * @param image Bitmap instance containing the downloaded image
         */
        fun onDownloadComplete(requestCode: Int, image: Bitmap)

        /**
         * Invoked when a download task failed
         * @param requestCode Identifier of the download request
         */
        fun onDownloadFailed(requestCode: Int)
    }


    /**
     * Internal class that represents a download task for a given image
     * It receives a requestCode that is used to identify each task.
     * Invokes ImageDownloadManager.onTaskFinish() once the download completes or fails.
     */
    class ImageDownloadTask(private val url: URL, val requestCode: Int) : Runnable {
        var bitmap: Bitmap? = null

        override fun run() {

            // Simulate a random network delay between 0 and 1.5 second
            Thread.sleep((Random.nextFloat() * 1500).toLong())

            // Simulate randomic download failures
            val shouldFail = Random.nextDouble(0.0, 2.0) < 0.5
            if (shouldFail) {
                ImageDownloadManager.onTaskFinish(this, DOWNLOAD_FAILED)
                return
            }

            val conn = url.openConnection() as HttpURLConnection
            conn.requestMethod = "GET"
            conn.doInput = true
            conn.connect()

            val inputStream = conn.inputStream

            bitmap = BitmapFactory.decodeStream(inputStream)
            conn.disconnect()
            inputStream.close()

            ImageDownloadManager.onTaskFinish(this, DOWNLOAD_COMPLETED)
        }
    }

}



